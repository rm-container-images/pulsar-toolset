FROM apachepulsar/pulsar-all

WORKDIR /tools

RUN curl -L https://github.com/mikefarah/yq/releases/download/v4.33.3/yq_linux_386 -o /tools/yq && \
    chmod +x -R /tools/*

COPY .bashrc /.bashrc
